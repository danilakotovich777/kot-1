package ex01;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import static org.junit.Assert.assertArrayEquals;

import junit.framework.Assert;

import java.io.IOException;

import ex01.Calc;

/**
 * Выполняет тестирование разработанных классов.
 *
 * @author Kot
 * @version 1.0
 */
public class MainTest {
    /**
     * Проверка основной функциональности класса {@linkplain Calc}
     */
    @Test
    public void testCalc() {
        Calc calc = new Calc();
        calc.init(10);
        assertArrayEquals(new String[]{"1010","12","a"}, calc.getResult().getNumbers());
        calc.init(20);
        assertArrayEquals(new String[]{"10100","24","14"}, calc.getResult().getNumbers());
        calc.init(30);
        assertArrayEquals(new String[]{"11110","36","1e"}, calc.getResult().getNumbers());
        calc.init(40);
        assertArrayEquals(new String[]{"101000","50","28"}, calc.getResult().getNumbers());
        calc.init(50);
        assertArrayEquals(new String[]{"110010","62","32"}, calc.getResult().getNumbers());
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() {
        Calc calc = new Calc();
        int decimalNumber;
        String[] numbers;
        for (int ctr = 0; ctr < 1000; ctr++) {
            decimalNumber = (int)( Math.random() * 500);
            numbers = calc.init(decimalNumber);
            try {
                calc.save();
            } catch (IOException e) {
                Assert.fail(e.getMessage());
            }
            calc.init((int)( Math.random() * 500));
            try {
                calc.restore();
            } catch (Exception e) {
                Assert.fail(e.getMessage());
            }
            assertArrayEquals(numbers, calc.getResult().getNumbers());

            assertEquals(decimalNumber, calc.getResult().getDecimalNumber());
        }
    }
}