package ex02;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import junit.framework.Assert;

import java.io.IOException;

import ex01.Item2d;

/**
 * Выполняет тестирование
 * разработанных классов.
 *
 * @author Kot
 * @version 2.0
 */
public class MainTest {
    /**
     * Проверка основной функциональности класса {@linkplain ViewResult}
     */
    @Test
    public void testCalc() {
        ViewResult view = new ViewResult(5);
        view.init(10);
        Item2d item = new Item2d();
        int ctr = 0;
        item.setAll(10, new String[]{"1010","12","a"});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(20, new String[]{"10100","24","14"});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(30, new String[]{"11110","36","1e"});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(40, new String[]{"101000","50","28"});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(50, new String[]{"110010","62","32"});
        assertTrue("expected:<" + item + "> but was:<" + view.getItems().get(ctr) + ">",
                view.getItems().get(ctr).equals(item));
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() {
        ViewResult view1 = new ViewResult(1000);
        ViewResult view2 = new ViewResult();
// Вычислим значение функции со случайным шагом приращения аргумента
        view1.init((int)(Math.random() * 500));
// Сохраним коллекцию view1.items
        try {
            view1.viewSave();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
// Загрузим коллекцию view2.items
        try {
            view2.viewRestore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
// Должны загрузить столько же элементов, сколько сохранили
        assertEquals(view1.getItems().size(), view2.getItems().size());
// Причем эти элементы должны быть равны.
// Для этого нужно определить метод equals
        assertTrue("containsAll()", view1.getItems().containsAll(view2.getItems()));
    }
}