package ex03;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import junit.framework.Assert;

import java.io.IOException;

import ex01.Item2d;

/**
 * Выполняет тестирование
 * разработанных классов.
 *
 * @author Kot
 * @version 3.0
 */
public class MainTest {
    /**
     * Проверка основной функциональности класса {@linkplain ViewTable}
     */
    @Test
    public void testCalc() {
        ViewTable tbl = new ViewTable(10, 5);
        assertEquals(10, tbl.getWidth());
        assertEquals(5, tbl.getItems().size());
        tbl.init(40, 10);
        Item2d item = new Item2d();
        int ctr = 0;
        item.setAll(10, new String[]{"1010","12","a"});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(20, new String[]{"10100","24","14"});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(30, new String[]{"11110","36","1e"});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(40, new String[]{"101000","50","28"});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
        ctr++;
        item.setAll(50, new String[]{"110010","62","32"});
        assertTrue("expected:<" + item + "> but was:<" + tbl.getItems().get(ctr) + ">",
                tbl.getItems().get(ctr).equals(item));
    }

    /**
     * Проверка сериализации. Корректность восстановления данных.
     */
    @Test
    public void testRestore() {
        ViewTable tbl1 = new ViewTable(10, 1000);
        ViewTable tbl2 = new ViewTable();
// Вычислим значение функции со случайным шагом приращения аргумента
        tbl1.init(30, (int)(Math.random() * 500));
// Сохраним коллекцию tbl1.items
        try {
            tbl1.viewSave();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
// Загрузим коллекцию tbl2.items
        try {
            tbl2.viewRestore();
        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }
// Должны загрузить столько же элементов, сколько сохранили
        assertEquals(tbl1.getItems().size(), tbl2.getItems().size());
// Причем эти элементы должны быть равны.
// Для этого нужно определить метод equals
        assertTrue("containsAll()", tbl1.getItems().containsAll(tbl2.getItems()));
    }
}