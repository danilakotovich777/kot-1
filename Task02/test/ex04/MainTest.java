package ex04;

import static org.junit.Assert.*;

import ex01.Calc;
import org.junit.Test;
import ex01.Item2d;
import ex02.ViewResult;

/**
 * Тестирование класса
 * ChangeItemCommand
 *
 * @author Kot
 * @version 4.0
 * @see ChangeItemCommand
 */
public class MainTest {
    /**
     * Проверка метода {@linkplain ChangeItemCommand#execute()}
     */
    @Test
    public void testExecute() {
        ChangeItemCommand cmd = new ChangeItemCommand();
        cmd.setItem(new Item2d());
        int decimalNumber, offset;
        Calc calc = new Calc();
        String[] numbers;
        for (int ctr = 0; ctr < 1000; ctr++) {
            cmd.getItem().setAll(decimalNumber = (int)(Math.random() * 100.0), numbers = calc.calc((int)(Math.random() * 100.0)));
            cmd.setOffset(offset = (int)(Math.random() * 100.0));
            cmd.execute();
            assertEquals(decimalNumber, cmd.getItem().getDecimalNumber());
            assertArrayEquals(calc.calc((int)(decimalNumber * offset)), cmd.getItem().getNumbers());
        }
    }

    /**
     * Проверка класса {@linkplain ChangeConsoleCommand}
     */
    @Test
    public void testChangeConsoleCommand() {
        ChangeConsoleCommand cmd = new ChangeConsoleCommand(new ViewResult());
        cmd.getView().viewInit();
        cmd.execute();
        assertEquals("'c'hange", cmd.toString());
        assertEquals('c', cmd.getKey());
    }
}