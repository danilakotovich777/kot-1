package ex01;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Хранит исходные данные и результат вычислений.
 *
 * @author Kot
 * @version 1.0
 */
public class Item2d implements Serializable {
    /**
     * Аргумент вычисляемой функции.
     */
// transient
    private int decimalNumber;
    /**
     * Результат вычисления функции.
     */
    private String[] numbers;


    /**
     * Автоматически сгенерированная константа
     */
    private static final long serialVersionUID = 1L;

    /**
     * Инициализирует поля {@linkplain Item2d#decimalNumber}, {@linkplain Item2d#numbers}
     */
    public Item2d() {
        decimalNumber = 0;
        numbers = new String[3];
    }

    /**
     * Устанавливает значения полей: аргумента
     * и результата вычисления функции.
     *
     * @param decimalNumber - значение для инициализации поля {@linkplain Item2d#decimalNumber}
     * @param numbers - значение для инициализации поля {@linkplain Item2d#numbers}
     */
    public Item2d(int decimalNumber, String[] numbers) {
        this.decimalNumber = decimalNumber;
        this.numbers = numbers;
    }

    /**
     * Установка значения поля {@linkplain Item2d#decimalNumber}
     *
     * @param decimalNumber - значение для {@linkplain Item2d#decimalNumber}
     * @return Значение {@linkplain Item2d#decimalNumber}
     */
    public int setDecimalNumber(int decimalNumber) {
        return this.decimalNumber = decimalNumber;
    }

    /**
     * Получение значения поля {@linkplain Item2d#decimalNumber}
     *
     * @return Значение {@linkplain Item2d#decimalNumber}
     */
    public int getDecimalNumber() {
        return decimalNumber;
    }

    /**
     * Установка значения поля {@linkplain Item2d#numbers}
     *
     * @param  - значение для {@linkplain Item2d#numbers}
     * @return Значение {@linkplain Item2d#numbers}
     */
    public String[] setNumbers(String[] numbers) {
        return this.numbers = numbers;
    }

    /**
     * Получение значения поля {@linkplain Item2d#numbers}
     *
     * @return значение {@linkplain Item2d#numbers}
     */
    public String[] getNumbers() {
        return numbers;
    }

    /**
     * Установка значений {@linkplain Item2d#decimalNumber} и {@linkplain Item2d#numbers}
     *
     * @param decimalNumber - значение для {@linkplain Item2d#decimalNumber}
     * @param numbers - значение для {@linkplain Item2d#numbers}
     * @return this
     */
    public Item2d setAll(int decimalNumber, String[] numbers) {
        this.decimalNumber = decimalNumber;
        this.numbers = numbers;
        return this;
    }

    /**
     * Представляет результат вычислений в виде строки.<br>{@inheritDoc}
     */
    @Override
    public String toString() {
        return "Decimal number = " + decimalNumber + ", bin = " + numbers[0] + ", oct = " + numbers[1] + ", hex = " + numbers[2];
    }

    /**
     * Автоматически сгенерированный метод.<br>{@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;


        Item2d other = (Item2d) obj;
        if (decimalNumber != other.decimalNumber)
            return false;
// изменено сравнение результата вычисления функции
        if (!Arrays.equals(numbers,other.numbers))
            return false;
        return true;
    }
}